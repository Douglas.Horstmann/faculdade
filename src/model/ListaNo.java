package model;

public class ListaNo<T> {

    private T dado;
    private ListaNo<T> proximo;
    private ListaNo<T> anterior;

    public void setDado(T dado) {
        this.dado = dado;
    }

    public T getDado() {
        return this.dado;
    }

    public void setProximo(ListaNo<T> no) {
        this.proximo = no;
    }

    public ListaNo<T> getProximo() {
        return this.proximo;
    }

	public ListaNo<T> getAnterior() {
		return anterior;
	}

	public void setAnterior(ListaNo<T> anterior) {
		this.anterior = anterior;
	}    
    
}
