package comparators;

import java.util.Comparator;

import model.Aluno;

public class SearchByAgeAndCity implements Comparator<Aluno> {

	public SearchByAgeAndCity() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compare(Aluno o1, Aluno o2) {
		if(o1.getIdade() == o2.getIdade() &&
			o1.getCidade().equals(o2.getCidade()))
		{
			return 1;
		}
		return 0;
	}

}
