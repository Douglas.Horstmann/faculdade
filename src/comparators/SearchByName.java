package comparators;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;

import model.Aluno;

public class SearchByName implements Comparator<Aluno> {
	
	public Aluno aluno = null;
	
	public SearchByName() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compare(Aluno o1, Aluno o2) {
		// TODO Auto-generated method stub
		if(o1.getNome().equals(o2.getNome())){
			return 1;
		}
		return 0;
	}
	
}
