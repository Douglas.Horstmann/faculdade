package datastructures;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;

import model.Aluno;
import model.ListaNo;
import comparators.SearchByName;

public class ListaEncadeada<T> {

    private ListaNo<Aluno> head;
    private ListaNo<Aluno> tail;
    //private int countNo = 0;
    
    // 
	public void append(T aluno) {
		
		ListaNo<Aluno> novoNo = new ListaNo<Aluno>();
		novoNo.setDado((Aluno)aluno);
		novoNo.setDado((Aluno)aluno);
		
		if(head == null) {
			addFirst(aluno);
		} else {
			tail.setProximo(novoNo);
			novoNo.setAnterior(tail);
			tail = novoNo;			
		}
		
		//countNo++;
		//System.out.println("Adicionado um novo n�: " + countNo);
		
	}

	public void addFirst(T dado) {

		ListaNo<Aluno> novoNo = new ListaNo<Aluno>();
		novoNo.setDado((Aluno)dado);				
		
		if (head == null) {
			head = novoNo;
			tail = novoNo;
		} else {
			head.setAnterior(novoNo);
			novoNo.setProximo(head);
			head = novoNo;
		}
    }

	@SuppressWarnings("unchecked")
	public T search(T key, Comparator<T> cmp)
	{
		ListaNo<Aluno> currentNo = head;
		String alunoNome = "";
		while(currentNo != null) {
			if(cmp.compare((T)currentNo.getDado(), key) == 1) {
				Aluno aluno = currentNo.getDado();
				alunoNome = aluno.getNome();
				//System.out.println("Encontrado aluno: " + aluno.getNome());	
			} 			
			currentNo = currentNo.getProximo();		
		}
		return (T)alunoNome;
	}	
	
	public void printObjects() {
		ListaNo<Aluno> currentNo = head;

        while (currentNo != null)
        {
            System.out.println(((Aluno)currentNo.getDado()).getNome());
            currentNo = currentNo.getProximo();
        }
    }
	
	@SuppressWarnings("unchecked")
	public static <T> ListaEncadeada<T> loadFromFile(FileReader arquivo) throws IOException {

		ListaEncadeada<Aluno> lista = new ListaEncadeada<Aluno>();
		
		BufferedReader buffer = new BufferedReader(arquivo);
		String linha;
		
		//int count = 0;
		
		while((linha = buffer.readLine()) != null) {
			String[] dadoAluno = linha.split(",");
			Aluno aluno = new Aluno();
			aluno.setMatricula(dadoAluno[0]);
			aluno.setNome(dadoAluno[1]);
			aluno.setEmail(dadoAluno[2]);
			aluno.setIdade(Integer.parseInt(dadoAluno[3]));
			aluno.setSexo(dadoAluno[4]);
			aluno.setEmpresa(dadoAluno[5]);
			aluno.setCidade(dadoAluno[6]);
			lista.append(aluno);
//			if(count == 999) {
//				System.out.println("999");
//			}
//			count++;
		}		
		
		return (ListaEncadeada<T>) lista;
	}

}
